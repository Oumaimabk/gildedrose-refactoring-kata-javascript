<h1 align="center">
  GildedRose Refactoring Kata
</h1>

<p align="center">
  <a href="#">
    <img src="https://img.shields.io/badge/version-1.0.0-green" alt="GitHub package.json version">
  </a>
  <a href="#" target="_blank" rel="noopener">
    <img src="https://img.shields.io/badge/language-javascript-yellow" alt="TEJP version">
  </a>
  <a href="#" target="_blank" rel="noopener">
    <img src=" https://img.shields.io/badge/tests-mocha-yellowgreen" alt="TEJP version">
  </a>
 
</p>

---

- [Installation](#installation)
- [Goals](#goals)
- [Choices](#choices)
- [Requirements](#requirements)

## Installation

### Set up

1. Just clone the project

```
git clone https://gitlab.com/Oumaimabk/gildedrose-refactoring-kata-javascript.git
```

2. Install the dependencies

```
npm install
```

### Tests

To test this project, just run the below command:

```
npm test
```

## Goals

The goal of the GildRose kata is to learn how to update the source code without changing the behaviour of the application. Refactoring helps you keep your code solid, dry, and easy to maintain.

### How to proceed?
* First, Make the change easy; Then, make the easy change.
- Write tests to  protect the behaviour of the legacy code.
- Once we have a good overview thanks to our tests, we can start to make changes 
  and know that we won't break it!
- Choose the best way to structure the code and refactor it.
- Implement another functionality after refactoring the code.
- Add tests for the new feature.
  
## design
- I chose the approach `copy/pase/delete` to structure my code.
    
        Separate the code into more function, Past all code into separated functions.
        The way we can clean this is walkimg through each one of the if statement in 
        these functions and looking at each one of the conditions in each of the if or the else 
        statement within this function and decide which one would we actually execute.
        Then we delete the non-executable code depending on the item.    

- I used also the design pattern `Decompose Conditional` to update my code.

        if/else statements can be a powerful tool when adding logic to your program.
        But they can also become unwieldy and confusing very quickly. One way to counteract 
        this is by making the conditional logic easier to understand by extracting it into 
        expressions that describe your intent.
        Plus, I was able to add the new functionality with ease.
  
- To have a cleaner codebase, I chose to put each part of the codebase in a dedicated file.


## Requirements

✔ NodeJS 
