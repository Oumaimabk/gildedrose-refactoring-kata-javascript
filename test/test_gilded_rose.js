var { expect } = require("chai");
var { Shop, Item } = require("../src/gilded_rose.js");
describe("Gilded Rose", function () {
  it("should foo", function () {
    const gildedRose = new Shop([new Item("foo", 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).to.equal("foo");
  });

  //  tests for normal items
  describe("Normal items", () => {
    it("should decrease the sellIn by date of all object", () => {
      const gildedRose = new Shop([
        new Item("Abc", 10, 5),
        new Item("Test", 9, 4),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].sellIn).to.equal(9);
      expect(items[1].sellIn).to.equal(8);
    });

    it("should decrease the sellIn by date", () => {
      const gildedRose = new Shop([new Item("Abc", 10, 5)]);
      const items = gildedRose.updateQuality();
      expect(items[0].sellIn).to.equal(9);
    });

    it("should set a negative sellIn by date when it has passed", () => {
      const gildedRose = new Shop([new Item("Test", 0, 8)]);
      const items = gildedRose.updateQuality();
      expect(items[0].sellIn).to.equal(-1);
    });

    it("should decrease the quality", () => {
      const gildedRose = new Shop([new Item("Test", 8, 4)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(3);
    });

    it("should decrease the quality by 2 if the sellIn by date has passed", () => {
      const gildedRose = new Shop([new Item("foo", 0, 7)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(5);
    });

    it("should always have a positive quality", () => {
      const gildedRose = new Shop([new Item("foo", 0, 0)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(0);
    });
  });

  //  tests for Aged Brie items
  describe("Aged Brie", function () {
    it("should decrease the sellIn of the item", () => {
      const gildedRose = new Shop([new Item("Aged Brie", 0, 8)]);
      const items = gildedRose.updateQuality();
      expect(items[0].sellIn).to.equal(-1);
    });

    it("should increase the quality after qualityUpdate", () => {
      const gildedRose = new Shop([new Item("Aged Brie", 20, 5)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(6);
    });

    it("should increase the quality two times faster if the item is expired", () => {
      const gildedRose = new Shop([new Item("Aged Brie", 0, 4)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(6);
    });

    it("should never increase the quality over 50", () => {
      const gildedRose = new Shop([new Item("Aged Brie", 20, 50)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(50);
    });
  });

  //  tests for Backstage passes items
  describe("Backstage passes to a TAFKAL80ETC concert", () => {
    it("should decrease the sellIn of the object", () => {
      const gildedRose = new Shop([
        new Item("Backstage passes to a TAFKAL80ETC concert", 0, 2),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].sellIn).to.equal(-1);
    });

    it("should decrease the quality if the sellIn by date is bigger than 10", () => {
      const gildedRose = new Shop([
        new Item("Backstage passes to a TAFKAL80ETC concert", 11, 5),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(6);
    });

    it("should increase the quality by 2 if it remains 10 days or lower", () => {
      const gildedRose = new Shop([
        new Item("Backstage passes to a TAFKAL80ETC concert", 10, 5),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(7);
    });

    it("should never increase the quality over 50 if it remains 10 days or lower", () => {
      const gildedRose = new Shop([
        new Item("Backstage passes to a TAFKAL80ETC concert", 10, 50),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(50);
    });

    it("should increase the quality by 3 if it remains 5 days or lower", () => {
      const gildedRose = new Shop([
        new Item("Backstage passes to a TAFKAL80ETC concert", 5, 5),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(8);
    });

    it("should increase the quality by 3 if it remains 1 day", () => {
      const gildedRose = new Shop([
        new Item("Backstage passes to a TAFKAL80ETC concert", 1, 5),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(8);
    });

    it("should never increase the quality over 50 if it remains 5 days or lower", () => {
      const gildedRose = new Shop([
        new Item("Backstage passes to a TAFKAL80ETC concert", 5, 50),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(50);
    });

    it("should set the quality to 0 after the date of the concert", () => {
      const gildedRose = new Shop([
        new Item("Backstage passes to a TAFKAL80ETC concert", 0, 5),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(0);
    });
  });
});
//  tests for Sulfuras items
describe("Sulfuras, Hand of Ragnaros", () => {
  it("should never decrease the sellIn by date", () => {
    const gildedRose = new Shop([
      new Item("Sulfuras, Hand of Ragnaros", 5, 80),
    ]);
    const items = gildedRose.updateQuality();
    expect(items[0].sellIn).to.equal(5);
  });

  it("should never decrease the quality", () => {
    const gildedRose = new Shop([
      new Item("Sulfuras, Hand of Ragnaros", 5, 80),
    ]);
    const items = gildedRose.updateQuality();
    expect(items[0].quality).to.equal(80);
  });

  //  tests for Conjured items
  describe("Conjured Mana Cake", () => {
    it("should decrease the sellIn by date of all object", () => {
      const gildedRose = new Shop([
        new Item("Conjured Mana Cake", 10, 5),
        new Item("Tata", 9, 4),
      ]);
      const items = gildedRose.updateQuality();
      expect(items[0].sellIn).to.equal(9);
      expect(items[1].sellIn).to.equal(8);
    });

    it("should decrease the sellIn by date", () => {
      const gildedRose = new Shop([new Item("Conjured Mana Cake", 10, 5)]);
      const items = gildedRose.updateQuality();
      expect(items[0].sellIn).to.equal(9);
    });

    it("should set a negative sellIn by date when it has passed", () => {
      const gildedRose = new Shop([new Item("Conjured Mana Cake", 0, 5)]);
      const items = gildedRose.updateQuality();
      expect(items[0].sellIn).to.equal(-1);
    });

    it("should decrease the quality twice", () => {
      const gildedRose = new Shop([new Item("Conjured Mana Cake", 10, 5)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(3);
    });

    it("should decrease the quality by 4 if the sellIn by date has passed", () => {
      const gildedRose = new Shop([new Item("Conjured Mana Cake", 0, 5)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(1);
    });

    it("should always have a positive quality", () => {
      const gildedRose = new Shop([new Item("Conjured Mana Cake", 0, 0)]);
      const items = gildedRose.updateQuality();
      expect(items[0].quality).to.equal(0);
    });
  });
});
