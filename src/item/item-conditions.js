const { MAX_QUALITY, MIN_QUALITY } = require("./item-constants");

const increaseQuality = (item) => {
  if (item.quality < MAX_QUALITY) {
    item.quality += 1;
  }
};
const decreaseQuality = (item) => {
  if (item.quality > MIN_QUALITY) {
    item.quality -= 1;
  }
};
const decreasetwiceQuality = (item) =>{
    if (item.quality > MIN_QUALITY) {
        item.quality -= 2;
      }
}
const decreaseSellIn = (item) => {
  item.sellIn -= 1;
};

const idExpired = (item) => {
  return item.sellIn < 0;
};

module.exports = {
  increaseQuality,
  decreaseQuality,
  decreaseSellIn,
  idExpired,
  decreasetwiceQuality
};
