const ItemTypes = {
    CHEESE: 'Aged Brie',
    TICKET :  'Backstage passes to a TAFKAL80ETC concert',
    LEGENDARY: 'Sulfuras, Hand of Ragnaros',
    CONJURED: "Conjured Mana Cake"
}  

const MAX_QUALITY = 50;
const MIN_QUALITY = 0;

module.exports = {
    ItemTypes,
    MAX_QUALITY,
    MIN_QUALITY
}