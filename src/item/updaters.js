const { MAX_QUALITY, ItemTypes } = require("./item-constants");
const {
  increaseQuality,
  decreaseQuality,
  decreaseSellIn,
  idExpired,
  decreasetwiceQuality,
} = require("./item-conditions");

const updateCheese = (item) => {
  increaseQuality(item);
  decreaseSellIn(item);
  idExpired(item) && increaseQuality(item);
};

const updateTicket = (item) => {
  if (item.quality < MAX_QUALITY) {
    item.quality += 1;
    item.sellIn < 11 && increaseQuality(item);
    item.sellIn < 6 && increaseQuality(item);
  }
  decreaseSellIn(item);
  item.quality = idExpired(item) ? 0 : item.quality;
};
const updateNormal = (item) => {
  decreaseQuality(item);
  decreaseSellIn(item);
  idExpired(item) && decreaseQuality(item);
};

const updateLegendary = (item) => {
  return item;
};

const updateConjured = (item) => {
  decreasetwiceQuality(item);
  decreaseSellIn(item);
  idExpired(item) && decreasetwiceQuality(item);
};
const updateSwitchedItem = (item) => {
  switch (item.name) {
    case ItemTypes.CHEESE:
      updateCheese(item);
      break;
    case ItemTypes.TICKET:
      updateTicket(item);
      break;
    case ItemTypes.LEGENDARY:
      updateLegendary(item);
      break;
    case ItemTypes.CONJURED:
      updateConjured(item);
      break;
    default:
      updateNormal(item);
      break;
  }
};

module.exports = {
  updateCheese,
  updateTicket,
  updateNormal,
  updateLegendary,
  updateSwitchedItem,
};
